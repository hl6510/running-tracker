import Vue from 'vue'
import Router from 'vue-router'
import * as Views from './views/index'

Vue.use(Router)

const routes = [
    {
        path: '/',
        name: 'home',
        component: Views.Home,
        props: route => ({ route })
    },
    {
        path: '/profile',
        name: 'profile',
        component: Views.Profile,
        props: route => ({ route })
    },
    {
        path: '/history',
        name: 'history',
        component: Views.History,
        props: route => ({ route })
    },
    {
        path: '/summary',
        name: 'summary',
        component: Views.Summary,
        props: route => ({ route })
    },
]

export default new Router({ routes })