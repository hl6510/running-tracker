export { default as Home } from './home/Home.vue'
export { default as Profile } from './profile/Profile.vue'
export { default as History } from './history/History.vue'
export { default as Summary } from './summary/Summary.vue'
